// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IAutenticacaoService } from '../providers.interfaces/IAutenticacaoService';
import { LoginModel } from '../models/LoginModel';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { HelloIonicConstantes } from '../app/helloIonicConstantes';
import { HttpClient } from '@angular/common/http';


/*
  Generated class for the ProvidersAutenticacaoServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AutenticacaoService implements IAutenticacaoService {

  constructor(
    public http: HttpClient
  ) {
    console.log('Hello ProvidersAutenticacaoServiceProvider Provider');
  }

  login(login: LoginModel): Observable<void> {

    if (!login || !login.email || !login.senha) {
      return Observable.throw('Email e/ou senha não informados');
    }

    let endpoint_url_api = HelloIonicConstantes.BASE_URL + '/' + HelloIonicConstantes.AUTH.LOGIN;

    let corpo_requisicao = {
      email: login.email,
      senha: login.senha
    };
    return this.http.post(endpoint_url_api, corpo_requisicao)
      .map( (response) => {
        // let resp = response.json();
      });
  }

  logout(): void {

  }
}
