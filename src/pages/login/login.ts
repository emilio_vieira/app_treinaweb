import { Component, Inject } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, LoadingController } from 'ionic-angular';
import { PaginaBase } from '../../infraestrutura/PaginaBase';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from '../../validadores/customValidators';
import { LoginModel } from '../../models/LoginModel';
import { Form } from '@angular/forms';
import { HomePage } from '../home/home';
import { IAutenticacaoService } from '../../providers.interfaces/IAutenticacaoService';
import { TabsPage } from '../tabs/tabs';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage extends PaginaBase {

  loginFrmGroup: FormGroup;
  foiSubmetido: boolean;
  loginModel: LoginModel;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    @Inject('IAutenticacaoService')
    public autenticacaoService: IAutenticacaoService // interfaces não são conhecidas em tempo de transpilacao. Precisam ser injetadas runtime com @inject() 
  ) {
    super(
      {
        formBuilder: formBuilder,
        alertCtrl: alertCtrl,
        loadingCtrl: loadingCtrl,
        toastCtrl: toastCtrl
      }
    );
    this.foiSubmetido = false;
    this.loginModel = new LoginModel();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  protected doCarregarValidadores(): void {
    // override method
    this.loginFrmGroup = this._formBuilder.group({
      email: ['', Validators.compose([Validators.required, CustomValidators.email])],
      senha: ['', Validators.compose([Validators.required, Validators.minLength(3)])]
    });
  }

  login() {

    this.foiSubmetido = true;
    this.esconderToast();

    if (this.loginFrmGroup.valid) {
      this.mostrarLoading('Efetuando Login...');
      this.autenticacaoService.login(this.loginModel)
        .subscribe(
          // lambdas
          data => {
            console.log(data);
            this.esconderLoading();
            // this.navCtrl.setRoot(HomePage, {}, { animate: true, direction: 'forward' });
            this.navCtrl.setRoot(TabsPage, {}, { animate: true, direction: 'forward' });
          },
          error => {
            this.esconderLoading();
            let msgErro: string = error.error.erro.mensagem;
            this.mostrarToast(msgErro);
          }
        );

    } else {
      alert('Dados incompletos ou invalidos');
    }
  }
}