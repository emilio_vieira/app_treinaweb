var express = require('express');
var app = express();
var cors = require('cors');
var bodyParser = require('body-parser');

// reconhecimentos
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); // GET
app.use(cors());

app.post('/api/login', function(req,resp){
    var email = req.body.email;
    var senha = req.body.senha;
    // erro
    if(email != 'emilio@sr2.uerj.br' || senha != '123456'){
        setTimeout(function(){
            // 401 unauthorized
            resp.send(401, {
                'erro': {
                    'http.code': 401,
                    'code': 'unauthorized',
                    'mensagem': 'Login e/ou senha inválidos'
                }
            });
        },1000)
    } else {
        // Authorized
        setTimeout(function(){
            // 200 OK
            resp
                .header('Access-Control-Allow-Origin', '*')
                .send(200,{
                    'data':{
                        'nome': 'Emílio Vieira',
                        'email': 'emilio@sr2.uerj.br',
                        'token': 'este_e_o_token'
                    }
                })
        },2000)
    }
});
app.listen(3000);
console.log('API está no ar');